obj-m := kinect.o

KDIR := /lib/modules/$(shell uname -r)/build/

EXTRA_CFLAGS := -I$(KDIR)/drivers/media/video/gspca/

all:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean

unload:
	-sudo rmmod kinect
	-sudo rmmod gspca_kinect
	-sudo rmmod gspca_main
load: all
	sudo modprobe gspca_main
	sudo insmod ./kinect.ko

reload: unload load
